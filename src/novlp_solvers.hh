template<class GV>
void novlp_solvers (const GV& gv, const int i, const int verb=0)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  // <<<2>>> Make grid function space
  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,Real,1> FEM;
  FEM fem(gv);
  typedef Dune::PDELab::NonoverlappingConformingDirichletConstraints<GV> CON;   // constraints class
  CON con(gv);
  typedef Dune::PDELab::istl::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem,con);
  gfs.name("solution");
  con.compute_ghosts(gfs);
  BCTypeParam bctype; // boundary condition type
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  cc.clear();
  Dune::PDELab::constraints(bctype,gfs,cc); // assemble constraints

  // some informations
  gfs.update();
  auto nrofdof =  gfs.globalSize();
  std::cout << "rank " << gv.comm().rank() << " number of DOF = " << nrofdof << std::endl;
  nrofdof = gv.comm().sum(nrofdof);
  if (gv.comm().rank()==0)
    std::cout << "number of DOF " << nrofdof << std::endl;

  // <<<3>>> Make grid operator space
  typedef ExampleLocalOperator<BCTypeParam> LOP;
  LOP lop(bctype);
  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
  MBE mbe(27); // Maximal number of nonzeroes per row can be cross-checked by patternStatistics().
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC,true> GO;
  GO go(gfs,cc,gfs,cc,lop,mbe);

  // <<<4>>> Make FE function with BC
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0); // initial value
  typedef BCExtension<GV,Real> G;      // boundary value + extension
  G g(gv);
  Dune::PDELab::interpolate(g,gfs,u);  // interpolate coefficient vector

  // <<<5>>> Select a linear solver backend and solve linear problem
  Dune::PDELab::DefaultTimeSource source;
  double start=source();



  if(i==0){
    typedef Dune::PDELab::ISTLBackend_NOVLP_CG_NOPREC<GFS> LS;
    LS ls(gfs,5000,verb);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}

  /*if(i==1){
    typedef Dune::PDELab::ISTLBackend_NOVLP_BCGS_Jacobi<GFS> LS;
    LS ls(gfs,5000,verb);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}*/

  if(i==2){
    typedef Dune::PDELab::ISTLBackend_NOVLP_CG_SSORk<GO> LS;
    LS ls(go,5000,5,verb);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}

  if(i==3){
    typedef Dune::PDELab::ISTLBackend_NOVLP_BCGS_SSORk<GO> LS;
    LS ls(go,5000,5,verb);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}

  if(i==4){
    typedef Dune::PDELab::ISTLBackend_NOVLP_CG_AMG_SSOR<GO,5> LS;
    LS ls(go,5000,verb);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}

  if (gv.comm().rank() == 0)
    std::cout<<"Total computation time was "<<source()-start
	     <<" seconds."<<std::endl;

  //visualize partitions
  typedef typename Dune::PDELab::BackendVectorSelector<GFS,Real>::Type V0;
  V0 partition(gfs,gv.comm().rank());
  typedef Dune::PDELab::DiscreteGridFunction<GFS,V0> DGF0;
  DGF0 pdgf(gfs,partition);

  // <<<6>>> graphical output
  Dune::VTKWriter<GV> vtkwriter(gv);
  if (gv.comm().size()>1) {
    typedef Dune::PDELab::VTKGridFunctionAdapter<DGF0> VTKADAPTER;
    vtkwriter.addCellData(std::make_shared<VTKADAPTER>(pdgf,"decomposition"));
  }

  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,u);
  if(i==0){
    vtkwriter.pwrite("novlp_cg_noprec","vtk","",Dune::VTK::appendedraw);}
  if(i==1){
    vtkwriter.pwrite("novlp_cg_jacobi","vtk","",Dune::VTK::appendedraw);}
  if(i==2){
    vtkwriter.pwrite("novlp_cg_ssork","vtk","",Dune::VTK::appendedraw);}
  if(i==3){
    vtkwriter.pwrite("novlp_bcgs_ssork","vtk","",Dune::VTK::appendedraw);}
  if(i==4){
    vtkwriter.pwrite("novlp_cg_amg_ssork","vtk","",Dune::VTK::appendedraw);}
}

