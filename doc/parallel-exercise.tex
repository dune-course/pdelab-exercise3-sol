%\documentclass[12pt,a4paper]{article}
\documentclass[american]{article}
\usepackage{float}
\usepackage{babel}
\usepackage[utf8]{inputenc}
\usepackage{uebungen}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{hyperref}

\title{\textbf{DUNE-PDELab Exercise 3\\Thursday, February~26\textsuperscript{th}~2015 \\11:15-12:45}}
\dozent{Rebecca de Cuveland}
\institute{IWR, University of Heidelberg}
\semester{}
\abgabetitle{}
\uebungslabel{Exercise}
\blattlabel{DUNE Workshop 2015 Exercise}

\begin{document}
\blatt{}{}

To parallelize computations we need a parallel grid with a
nonoverlapping or overlapping grid partition and a corresponding
solver, see for an overview the pdelab doxygen documentation
\url{http://www.dune-project.org/doc-pdelab-master/doxygen/html/group__Backend.html}

The parallel grid managers are:
\begin{enumerate}
\item \emph{YaspGrid}: grid manager for structured grids with either a
  nonoverlapping or overlapping partitioning.

\item \emph{ALUGrid}: grid manager for unstructured grids with
  nonoverlapping partitioning (is only parallel in 3D!).

\item \emph{UG}: grid manager for unstructured grids with
  nonoverlapping partitioning.
\end{enumerate}

\emph{Implement a VTK output of the vertex-pid mapping:}\\ To
visualize the association of each vertex with a process id, it is
convenient to write a VTK file for \textit{ParaView}. Notice, that the
\texttt{VTKWriter} does not need any special treatment in the parallel
case. When applied to a \texttt{GridView} of a parallel grid, each
process will produce output corresponding to his local sub grid
(\texttt{*.vtu} files). Furthermore, the process of zero id will
create a global file (\texttt{*.pvtu}) which corresponds to the global
grid.

\emph{Hints for the Parallel DUNE Excercise:}
\begin{itemize}
\item Parallel DUNE programs utilize the \textit{Message Parsing
  Interface} MPI. However, many different MPI implementations are
  known to work well with DUNE.

\item With cmake your modules are by default built for the parallel use.

\item To actually run the parallel program, you need to call the
  \texttt{mpirun} command of your MPI implementation (in this computer
  pool we use Open MPI). For example the command
\begin{verbatim}
   mpirun -np 2 ./pdelab_exercise3
\end{verbatim}
  within your \texttt{exercise} directory will run the application
  with two parallel processes. The pool computer has a quad core CPU,
  so you can not expect a speedup of more than four.
\end{itemize}

\newpage
\begin{uebung}{\bf Lineare solvers and optimization}
  %{\bf Example 1a)} \texttt{pdelab\_exercise3.cc} in
  \texttt{pdelab-exercise3/src/} solves the Laplace Equation with Q1
  elements with only one process. The program is very similar to
  example02 from the second pdelab exercise. As a linear solver, the
  stabilized CG method with SSOR as preconditioner is used
  (\texttt{ISTLBackend\_SEQ\_BCGS\_SSOR}). It is defined in
  \texttt{seq\_solvers.hh}. (Note that you have to create a
  \texttt{vtk} folder prior to running the program. Otherwise your
  program will abort!)

The program's output is:
\begin{verbatim}
SEQ_BCGS_SSOR
number of DOF =1331
=== matrix setup (max) 0.052003 s
=== matrix assembly (max) 1.04407 s
=== residual assembly (max) 0.132008 s
=== solving (reduction: 1e-10) === BiCGSTABSolver
 Iter          Defect            Rate
    0            0.027
  0.5       0.00417168         0.154507
    1      0.000538021          0.12897
  1.5      4.45227e-06       0.00827527
    2      1.25853e-07        0.0282671
  2.5      1.51266e-09        0.0120193
    3      6.55027e-11         0.043303
  3.5      3.14141e-11         0.479585
    4      1.64232e-12        0.0522797
=== rate=0.00279269, T=0.136008, TIT=0.034002, IT=4
0.136008 s
Total computation time was 1.36573 seconds.
\end{verbatim}

For each iteration of the linear solver you get the \emph{defect} $r^n$ =
$Au^n-b$ of your algebraic problem and the convergence rate (the
reduction of your defect) $\|b-Ax^n\|/\|b-Ax^{n-1}\|$.  The last line
displays the average convergence rate (\emph{rate}), the time needed
for the iterations (\emph{T}), average time for one iteration
(\emph{TIT}) and the number of iterations (\emph{IT}).  You can change
the amount of output through the verbosity parameter ($0:$ print
nothing - $2:$ print for every iteration), for example
\texttt{ISTLBackend\_SEQ\_BCGS\_SSOR}:
\begin{verbatim}
typedef Dune::PDELab::ISTLBackend_SEQ_CG_SSOR LS; 
int verbosity = 0;
LS ls(5000,verbosity);
\end{verbatim}
Your code is provides two cmake build directories,
\texttt{release-build} is compiled with optimization with\\
\texttt{-DCMAKE\_CXX\_FLAGS\_RELEASE='-O3 -DNDEBUG -g0
  -Wno-deprecated-declarations -funroll-loops'}\\ and
\texttt{debug-build} is compiled without optimization with
\\\texttt{-DCMAKE\_CXX\_FLAGS\_DEBUG='-O0 -ggdb -Wall'},\\ which is useful for
debugging but the program is slow.  Make sure your are in the
\texttt{release-build} directory. You can also compare the computation
time with and without optimization.
\end{uebung}

\begin{uebung}{\bf Parallel implementation, Overlapping Case}
 
Adapt \texttt{ovlp\_solvers.hh} (which is now essentially a copy of
\texttt{seq\_solvers.hh}) to work in parallel with overlapping
solvers.  Therefore you have to take care of the following points:
\begin{itemize}
  \item Use an overlapping grid
  \item Use appropriate constraints
    (\texttt{OverlappingConformingDirichletConstraints})
  \item Use a parallel overlapping solver
\end{itemize} 
\end{uebung}

\begin{uebung}{\bf Parallel implementation, Nonoverlapping Case}
Adapt \texttt{novlp\_solvers.hh} to work in parallel with noverlapping
solvers. Therefore you have to take care of the following points:
\begin{itemize}
  \item Use a noverlapping grid
  \item Use appropriate constraints
    (\texttt{NonoverlappingConformingDirichletConstraints}) and call
    the constraints method \texttt{compute\_ghosts} after
    instantiation of the constraints object (needs the
    GridFunctionSpace as argument)
  \item Set the GridOperator template argument
    \texttt{nonoverlapping\_mode} to true
  \item Use a parallel nonoverlapping solver (the solver with Jacobi preconditioner does not work right now)
\end{itemize}

Uncomment the corresponding lines in \texttt{pdelab\_exercise3.cc} and don't
forget to use the \texttt{mpirun} command, otherwise you will still
solve the problem sequentially!

Notice the different convergence behaviour of the different
solvers. Try using solvers with or without preconditioners. Especially
solvers preconditioned with AMG (Algebraic MultiGrid) lead to very
good convergence rates, because AMG scales almost linearly to the
numer of unknowns.

Compare the convergence rates and calculation time between the
sequential and parallel case. Augment the size of the grid (for
example to \texttt{64x64x64} or \texttt{128x128x128} cells) to observe the influence of
parallelization on bigger grids (only with optimization).
\end{uebung}

\begin{uebung}{\bf Unstructured Grids}
%\subsection{ALU Cube Grid}
There's an example for an unstructured grid in
\texttt{grids/complex.dgf}. Create an ALU grid
(\texttt{ALUCubeGrid<3,3>}) from this file in
\texttt{pdelab\_exercise3.cc} and use it with a nonoverlapping
solver. Your grid has to call the method \texttt{loadBalance()}
previous to creation of the \texttt{GridView} to distribute it to the
different processes.

Look at the distribution of the grid to your processes with paraview.
ALU and UG use the graph partitioning tool \emph{METIS} to partition
the mesh. As the partitioning can slightly change with each run your
convergence rates can do so too.
\end{uebung}

\end{document}
%%% Local Variables: 
%%% mode: latex
%%% mode: TeX-PDF
%%% mode: auto-fill
%%% TeX-master: t
%%% mode: flyspell
%%% ispell-local-dictionary: "american"
%%% End: 
