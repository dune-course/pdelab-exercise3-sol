template<class GV>
void seq_solvers (const GV& gv,const int i)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  // <<<2>>> Make grid function space
  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,Real,1> FEM;
  FEM fem(gv);
  typedef Dune::PDELab::ConformingDirichletConstraints CON;   // constraints class
  CON con;
  typedef Dune::PDELab::istl::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem,con);
  gfs.name("solution");
  BCTypeParam bctype; // boundary condition type
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  cc.clear();
  Dune::PDELab::constraints(bctype,gfs,cc); // assemble constraints

  // some information
  gfs.update();
  std::cout << "number of DOF =" << gfs.globalSize() << " of " << cc.size()<< std::endl;

  // <<<3>>> Make grid operator space
  typedef ExampleLocalOperator<BCTypeParam> LOP;
  LOP lop(bctype);
  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
  MBE mbe(27); // Maximal number of nonzeroes per row can be cross-checked by patternStatistics().
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
  GO go(gfs,cc,gfs,cc,lop,mbe);

  // How well did we estimate the number of entries per matrix row?
  // => print Jacobian pattern statistics
  typename GO::Traits::Jacobian jac(go);
  std::cout << jac.patternStatistics() << std::endl;

  // <<<4>>> Make FE function with BC
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0); // initial value
  typedef BCExtension<GV,Real> G;      // boundary value + extension
  G g(gv);
  Dune::PDELab::interpolate(g,gfs,u);  // interpolate coefficient vector

  // <<<5>>> Select a linear solver backend and solve problem
  Dune::PDELab::DefaultTimeSource source;
  double start=source();
  if(i==0){

    typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
    LS ls(5000,2);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}

  if(i==1){
    typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_AMG_SSOR<GO> LS;
    LS ls(5000,1,true);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}

  if (gv.comm().rank() == 0)
    std::cout<<"Total computation time was "<<source()-start
	     <<" seconds."<<std::endl;

  // <<<6>>> graphical output
  Dune::VTKWriter<GV> vtkwriter(gv);
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,u);
      if(i==0){
      vtkwriter.pwrite("seq_bcgs_ssor","vtk","",Dune::VTK::appendedraw);}
    if(i==1){
      vtkwriter.pwrite("seq_bcgs_amg","vtk","",Dune::VTK::appendedraw);}
}
