template<class GV>
void ovlp_solvers (const GV& gv, const int i, const int verb=0)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;
  
  // <<<2>>> Make grid function space
  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,Real,1> FEM;
  FEM fem(gv);
  typedef Dune::PDELab::OverlappingConformingDirichletConstraints CON;   // constraints class for overlapping problem
  CON con;
  typedef Dune::PDELab::istl::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem,con);
  gfs.name("solution");
  BCTypeParam bctype; // boundary condition type
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  cc.clear();
  Dune::PDELab::constraints(bctype,gfs,cc); // assemble constraints

  // some informations
  gfs.update();
  auto nrofdof =  gfs.globalSize();
  std::cout << "rank " << gv.comm().rank() << " number of DOF = " << nrofdof << std::endl;
  nrofdof = gv.comm().sum(nrofdof);
  if (gv.comm().rank()==0)
    std::cout << "number of DOF " << nrofdof << std::endl;

  // <<<3>>> Make grid operator space
  typedef ExampleLocalOperator<BCTypeParam> LOP;
  LOP lop(bctype);
  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
  MBE mbe(27); // Maximal number of nonzeroes per row can be cross-checked by patternStatistics().
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
  GO go(gfs,cc,gfs,cc,lop,mbe);

  // <<<4>>> Make FE function with BC
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0); // initial value
  typedef BCExtension<GV,Real> G;      // boundary value + extension
  G g(gv);
  Dune::PDELab::interpolate(g,gfs,u);  // interpolate coefficient vector


  // <<<5>>> Select a linear solver backend and solve problem
  Dune::PDELab::DefaultTimeSource source;
  double start=source();
  if(i==0){
    typedef Dune::PDELab::ISTLBackend_OVLP_BCGS_SSORk<GFS,CC> LS;
    LS ls(gfs,cc,5000,5,verb);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}

  if(i==1){
    typedef Dune::PDELab::ISTLBackend_OVLP_BCGS_ILU0<GFS,CC> LS;
    LS ls(gfs,cc,5000,verb);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}

  if(i==2){
    typedef Dune::PDELab::ISTLBackend_OVLP_CG_SSORk<GFS,CC> LS;
    LS ls(gfs,cc,5000,5,verb);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}

  if(i==3){
    typedef Dune::PDELab::ISTLBackend_OVLP_GMRES_ILU0<GFS,CC> LS;
    LS ls(gfs,cc,5000,verb);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}

  if(i==4){
    typedef Dune::PDELab::ISTLBackend_BCGS_AMG_SSOR<GO> LS;
    LS ls(gfs,2,5000,verb);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}
  
  if(i==5){
    typedef Dune::PDELab::ISTLBackend_CG_AMG_SSOR<GO> LS;
    LS ls(gfs,2,5000,verb);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();}

  if (gv.comm().rank() == 0)
    std::cout<<"Total computation time was "<<source()-start
	     <<" seconds."<<std::endl;
  
  //visualize partitions
  typedef typename Dune::PDELab::BackendVectorSelector<GFS,Real>::Type V0;
  V0 partition(gfs,gv.comm().rank());
  typedef Dune::PDELab::DiscreteGridFunction<GFS,V0> DGF0;
  DGF0 pdgf(gfs,partition);

  // <<<6>>> graphical output
  Dune::VTKWriter<GV> vtkwriter(gv);
  if (gv.comm().size()>1) {
    typedef Dune::PDELab::VTKGridFunctionAdapter<DGF0> VTKADAPTER;
    vtkwriter.addCellData(std::make_shared<VTKADAPTER>(pdgf,"decomposition"));
  }
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,u);


  if(i==0){
    vtkwriter.pwrite("ovlp_bcgs_ssork","vtk","",Dune::VTK::appendedraw);}
  if(i==1){
    vtkwriter.pwrite("ovlp_bcgs_ilu0","vtk","",Dune::VTK::appendedraw);}
  if(i==2){
    vtkwriter.pwrite("ovlp_cg_ssork","vtk","",Dune::VTK::appendedraw);}
  if(i==3){
    vtkwriter.pwrite("ovlp_gmres_ilu0","vtk","",Dune::VTK::appendedraw);}
  if(i==4){
    vtkwriter.pwrite("ovlp_bcgs_amg","vtk","",Dune::VTK::appendedraw);}
  if(i==5){
    vtkwriter.pwrite("ovlp_cg_amg","vtk","",Dune::VTK::appendedraw);}
}
